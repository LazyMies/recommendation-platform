# Assignment 4

### Project repository
https://gitlab.com/LazyMies/recommendation-platform
Require nodejs version 14
Npm version 6.14.*
Installation step:

 1. Clone the project https://gitlab.com/LazyMies/recommendation-platform.git
 2. Install the project by running `npm install`
 3. Start the server by running `npm start`

The project is ready to view at `http://localhost:3000`

### Info on the special data
Common data types like string, number, boolean are easy to understand. However, due to a technical issue in converting the Map object to JSON, I decided to use a default object instead of Map for retrieving data. 

`movies` is a map object mapping the `movieId` to an object type `Movie`. Here is an example of the `movies` map object.
```JSON
{
	"1": {
  		"movieId": "1",
  		"title": "Toy Story (1995)",
  		"genres": "Adventure|Animation|Children|Comedy|Fantasy"
	}
}
```

Similar to `movies`, `usersMap` is also a map object mapping the `userId` to an object type `User`. Here is an example of the `usersMap` map object.

```JSON
{
	"1": {
		"userId": "1",
  		"movieRateMap": {
			"1": 4,
			"3": 4,
			"6": 4,
			"47": 5,
			"50": 5,
			"70": 3
		}
	},
}
```

### Why-Not Group Recommendation
Test page: http://localhost:3000/group-recommendation/why-not
Implementation

### Initialise a list of movies to a group
Before calculating a group recommendation list, 50 `movieIDs` are randomly generated and are verified existing in the movie database to avoid null result.
Example of randomise `movieId`

```JSON
  "movieIds": [
    "167634",
    "128620",
    "380",
    "493",
    "8782",
    "1690",
    "8831",
    "79006",
    "802",
    "1546",
    "1296",
    "48262",
    "7366",
    "5938",
    "152173",
    "53993",
    "48741",
    "31737",
    "109576",
    "54908",
    "6234",
    "17",
    "3105",
    "69644",
    "7282",
    "775",
    "165959",
    "4710",
    "51666",
    "90357"
  ]
```

### Fairness in Group Recommendation
To build the a recommended list for a group considering the fairness. Here is the strategy.

1. Build an original recommened list with `Average` aggregation method. At this point, the list still has 30 items from the randomise `movieIds`.
2. Sort the list in ascendency order and only take 20 first item in the list. 
3. Calulate the fairness for each user in the group by dividing the (total number of movie in the group list having average rate greater or equal the same user's rate on that movie) with the total number movies in the list.
4. Calulate the new movie rate for each user by applying the formular.
   

5. Recalulate the group average aggregation list with the new rate with fairness.

### Atomic case (e.g., Why not Matrix)
```ts
async whyNotRecommendMovieToUser(userId: string, movieId: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotAtomResult> {
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId);
        if (usersRatedMovie.length == 0) {
            return {
                result: `No one has rated this item`,
                tuples: []
            };
        }
        

        const avgRateUsersRecord: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId);
        // this is the Peers data
        let simUsers : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecord, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);

        // there is no one in the Peers
        if (simUsers.length == 0) {
            return {
                result: `None of your peers has rated this item.`,
                tuples: []
            };
        }

        // there are 1 or 2 users in the Peers
        if (simUsers.length <= 2) {
            return {
                result: `Only ${simUsers.length} (< 3) of user ${userId} peers has rated movie ${movieId}.`,
                tuples: []
            };
        }

        // sort the Peers and take the three (can be more , but in this case 3 is the simplified implementation)
        simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        // Generate the tuple data (userId, prediction data, similarity with the current User)
        const tuples = await Promise.all(
            simUsers.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        let result: string = '';

        // Setting the value that considering users like the movie is 3.5
        // Find all tuples with prediction data greater than 3.5
        const likeTuplesFilter = tuples.filter(tuple => tuple.score >= this.LIKE_THRESHOLD);

        if (likeTuplesFilter.length == 0) {
            result = 'All of your peers have given the item a low score.';
        } else if (likeTuplesFilter.length < tuples.length) {
            result = `${likeTuplesFilter.length} like ${movieId}, but ${tuples.length - likeTuplesFilter.length} dislike it`;
        }

        return {
            result,
            tuples
        };
    }
```

### Atomic Case (e.g., Why Not A But B)
```ts
async whySuggestBButNotA(userId: string, movieId1: string, movieId2: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotButResult> {
        // check if all users rated the certain movie
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId1);
        if (usersRatedMovie.length == 0) {
            return {
                result: `No one has rated this item ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }
        
        // find all Peers for movie1
        const avgRateUsersRecordMv1: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId1);
        let simUsersMv1 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv1, this.simWithNormalisedAvg);

        // find all Peers for movie2
        const avgRateUsersRecordMv2: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId2);
        let simUsersMv2 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv2, this.simWithNormalisedAvg);
        

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsersMv1 = simUsersMv1.filter(simUser => simUser['sim'] > 0);
        simUsersMv2 = simUsersMv2.filter(simUser => simUser['sim'] > 0);
        
        if (simUsersMv1.length == 0) {
            return {
                result: `Your most similar peers has not rated ${movieId1} but have rated ${movieId2}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        if (simUsersMv1.length <= 2) {
            return {
                result: `${simUsersMv2.length} of your peers have rated item ${movieId2} but only ${simUsersMv1.length} < (3) has rated item ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        simUsersMv1 = simUsersMv1
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        // should not cause error because it is a highly rated mv
        simUsersMv2 = simUsersMv2
        .sort((a, b) => +b['sim'] - +a['sim'])
        .slice(0, 3);

        const tuplesMv1 = await Promise.all(
            simUsersMv1.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId1, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        const tuplesMv2 = await Promise.all(
            simUsersMv2.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId2, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        return {
            result : `${simUsersMv2.length} peers like item ${movieId2} and ${simUsersMv1.length} dislike item ${movieId1}`,
            tuplesMv1,
            tuplesMv2
        };
    }
```

### Why-Not group (e.g., Why not action movies?)
It is challenging in this case because a movie has multiple genres `Adventure|Animation|Children|Comedy|Fantasy`. It is complicated to consider all genres for a movie. I decided to use the first item as a main genre. 

```ts
async whyNotMovieGenre(userId: string, movieGenre: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotMovieTypeResult> {
        // Find all users rated movie with a certain genre
        const usersRatedMovieGenre: Array<UserId> = getUserRatedMovieType(usersRecord, moviesRecord, userId, movieGenre);
        if (usersRatedMovieGenre.length == 0) {
            return {
                result: `${movieGenre} hasnt had any movie rated`,
                tuples: []
            };
        }

        // find a movie id of that genre rated by the user
        const movie: Movie = 
            Object
                .keys(usersRecord[userId].movieRateRecord)
                .map((mvId: string) => moviesRecord[mvId])
                .find((currentMovie: Movie) => {
                    const currentMovieGenre = currentMovie.genres.indexOf('|') > -1 ? currentMovie.genres.split('|')[0] : currentMovie.genres;
                    return currentMovieGenre.indexOf(movieGenre) > -1;
                });
        if (!movie) {
            return {
                result: `You hasn't rated genre ${movieGenre}`,
                tuples: []
            };
        }

        const movieId = movie.movieId;
        const avgRateUsersRecord: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId);
        // this is the Peers data
        let simUsers : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovieGenre, moviesRecord, avgRateUsersRecord, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);
        if (simUsers.length == 0) {
            return {
                result: `None of your peers has rated ${movieGenre}`,
                tuples: []
            };
        }

        if (simUsers.length <= 2) {
            return {
                result: `Only ${simUsers.length} (< 3) of user ${userId} peers has rated movie ${movieGenre}.`,
                tuples: []
            };
        }

        // sort the Peers and take the three (can be more , but in this case 3 is the simplified implementation)
        simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);


        // Generate the tuple data (userId, prediction data, similarity with the current User)
        const tuples = await Promise.all(
            simUsers.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        let result: string = '';
        // Setting the value that considering users like the movie is 3.5
        // Find all tuples with prediction data greater than 3.5
        const likeTuplesFilter = tuples.filter(tuple => tuple.score >= this.LIKE_THRESHOLD);

        if (likeTuplesFilter.length == 0) {
            result = `Your peers dislike ${movieGenre}`;
        } else if (likeTuplesFilter.length < tuples.length) {
            result = `Only ${likeTuplesFilter.length} like ${movieGenre}.`;
        }

        return {
            result,
            tuples
        };
    }
```

### Why-Not Position absenteeism (e.g., Why not rank Matrix first?)
The calculation for this question is based on two aspects 
1. The result from CF.
2. The aggregation strategy.


The result from CF is similar to the why-not-atom question. Below is the implemation for the CF
```ts
async whyNotRankHigher(userId: string, movieId: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotRankHigherResult> {
        // checking if all users rated movies
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId);
        if (usersRatedMovie.length == 0) {
            return {
                result: `No one has rated this item`,
                tuples: []
            };
        }
        
        const avgRateUsersRecord: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId);
        // Finding Peers for current users based on users rated the movie
        let simUsers : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecord, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);
        if (simUsers.length == 0) {
            return {
                result: `None of your peers has rated this item.`,
                tuples: []
            };
        }

        if (simUsers.length <= 2) {
            return {
                result: `Only ${simUsers.length} (< 3) of user ${userId} peers has rated movie ${movieId}.`,
                tuples: []
            };
        }

        simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        const tuples = await Promise.all(
            simUsers.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        let result: string = '';
        const likeTuplesFilter = tuples.filter(tuple => tuple.score >= this.LIKE_THRESHOLD);

        if (likeTuplesFilter.length == 0) {
            result = `${simUsers.length} of your peers have given a low score to this item`;
        } else if (likeTuplesFilter.length < tuples.length) {
            result = `${likeTuplesFilter.length} like ${movieId}, but ${tuples.length - likeTuplesFilter.length} dislike it`;
        }

        return {
            result,
            tuples
        };
    }
```

There can be a case that the user and its peers extremely likes the movie and rated it with high value (>=4), but the movie was not ranked high in the group list. A CF-atom-why-not is enough because it is personal result. The result needs to consider the rating of other members in the group to provide a better explanation. Along with the result from the method above, the groupResult is added to the data so display the comparision of the user movie rate to the others.
```ts
whyNotRankHigherGroup(userId: string, movieId: string, groupListData: WhyNotGroupListData, whyNotRankHigher: WhyNotRankHigherResult) : WhyNotRankHigherResultGroup{
        // initialise array of user rating that certain movie id
        const userMovieRates = groupListData.listOfUsers.map(user => {
            return {
                userId: user.userId,
                movieId: movieId,
                rate: user.preds[movieId]
            };
        });

        let groupResult = '';

        // sort by the rate value in ascendency order
        userMovieRates.sort((a, b) => b.rate - a.rate);

        
        if (userMovieRates[0].userId === userId) {
            // if the current user is in the first
            groupResult = `Other users did not rate the movie ${movieId} as high as you.`;
        } else if (userMovieRates[userMovieRates.length - 1]) {
            // if the current user is in the first
            groupResult = `You did not rate the movie ${movieId} as high as other group members.`;
        } else {
            // in the middle
            groupResult = `The averate rage for the movie ${movieId} is not as high enough`;
        }
        
        return {
            ...whyNotRankHigher,
            groupResult
        }
    }
```

### Why not B ranked higher than A
```ts
async whyisNotAHigherThanB(userId: string, movieId1: string, movieId2: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotRankHigherThanResult> {
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId1);
        if (usersRatedMovie.length == 0) {
            return {
                result: `Your peers have rated ${movieId2} but none of them have rated ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }
        
        const avgRateUsersRecordMv1: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId1);
        let simUsersMv1 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv1, this.simWithNormalisedAvg);

        const avgRateUsersRecordMv2: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId2);
        let simUsersMv2 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv2, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsersMv1 = simUsersMv1.filter(simUser => simUser['sim'] > 0);
        simUsersMv2 = simUsersMv2.filter(simUser => simUser['sim'] > 0);
        
        if (simUsersMv1.length == 0) {
            return {
                result: `Your most similar peers has not rated ${movieId1} but have rated ${movieId2}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        if (simUsersMv1.length <= 2) {
            return {
                result: `${simUsersMv2.length} of your peers have rated item ${movieId2} but only ${simUsersMv1.length} < (3) has rated item ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        simUsersMv1 = simUsersMv1
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        // should not cause error because it is a highly rated mv
        simUsersMv2 = simUsersMv2
        .sort((a, b) => +b['sim'] - +a['sim'])
        .slice(0, 3);

        const tuplesMv1 = await Promise.all(
            simUsersMv1.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId1, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        const tuplesMv2 = await Promise.all(
            simUsersMv2.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId2, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );
        
        return {
            result : `${simUsersMv2.length} peers like item ${movieId2} and ${simUsersMv1.length} dislike item ${movieId1}`,
            tuplesMv1,
            tuplesMv2
        };
    }
```