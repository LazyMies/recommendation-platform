# Assignment 1

### Project repository
https://gitlab.com/LazyMies/recommendation-platform
Require nodejs version 14
Npm version 6.14.*
Installation step:

 1. Clone the project https://gitlab.com/LazyMies/recommendation-platform.git
 2. Install the project by running `npm install`
 3. Start the server by running `npm start`

The project is ready to view at `http://localhost:3000`

### Info on the special data
Common data types like string, number, boolean are easy to understand. However, due to a technical issue in converting the Map object to JSON, I decided to use a default object instead of Map for retrieving data. 

`movies` is a map object mapping the `movieId` to an object type `Movie`. Here is an example of the `movies` map object.
```JSON
{
	"1": {
  		"movieId": "1",
  		"title": "Toy Story (1995)",
  		"genres": "Adventure|Animation|Children|Comedy|Fantasy"
	}
}
```

Similar to `movies`, `usersMap` is also a map object mapping the `userId` to an object type `User`. Here is an example of the `usersMap` map object.

```JSON
{
	"1": {
		"userId": "1",
  		"movieRateMap": {
			"1": 4,
			"3": 4,
			"6": 4,
			"47": 5,
			"50": 5,
			"70": 3
		}
	},
}
```


### Display first few rows & total count
Result page: http://localhost:3000/ratings

## user-based collaborative filtering approach

### Peason correlation for computing similarities between users
Test page: http://localhost:3000/user-recommendation/user-based
Implementation

```ts
async sim(userAId: string, userBId: string, movies: object, usersMap: object) {
        let dividend: number = 0;
        let divisorA: number = 0;
        let divisorB: number = 0;
        const userA: User = usersMap[userAId];
        const userB: User = usersMap[userBId];

        const averateRatingUserA: number = userA.getAverateRating();
        const averateRatingUserB: number = userB.getAverateRating();

        for (const movieId in movies) {
            const movie = movies[movieId];
            if (userA.hasRatedMovie(movie) && userB.hasRatedMovie(movie)) {
                const rateOfUserA = userA.getMovieRate(movie);
                const rateOfUserB = userB.getMovieRate(movie);
                dividend += (rateOfUserA - averateRatingUserA) * (rateOfUserB - averateRatingUserB);
                divisorA += (rateOfUserA - averateRatingUserA) * (rateOfUserA - averateRatingUserA);
                divisorB += (rateOfUserB - averateRatingUserB) * (rateOfUserB - averateRatingUserB);
            }
        }

        if (dividend != 0 && divisorA != 0 && divisorB != 0) {
            const divisor = Math.sqrt(divisorA)*Math.sqrt(divisorB);
            return +(dividend / divisor).toFixed(4);
        }

        return 0;
    }
```

### Prediction function for calculating missing movies scores
Test page: http://localhost:3000/user-recommendation/item-based
Implementation
```ts
// the sim prediction between userA and userB with the normalized rating value
async simWithNormalisedAvg(userAId: string, userBId: string, movies: object, usersMap: object) {
        let dividend: number = 0;
        let divisorA: number = 0;
        let divisorB: number = 0;
        const userA: User = usersMap[userAId];
        const userB: User = usersMap[userBId];

        for (const movieId in movies) {
            const movie = movies[movieId];
            if (userA.hasRatedMovie(movie) && userB.hasRatedMovie(movie)) {
                const rateOfUserA = userA.getMovieRate(movie);
                const rateOfUserB = userB.getMovieRate(movie);
                dividend += (rateOfUserA * rateOfUserB);
                divisorA += Math.pow(rateOfUserA, 2);
                divisorB += Math.pow(rateOfUserB, 2);
            }
        }

        if (dividend != 0 && divisorA != 0 && divisorB != 0) {
            const divisor = Math.sqrt(divisorA)*Math.sqrt(divisorB);
            return +(dividend / divisor).toFixed(4);
        }

        return 0;
    }

    async predUserMovie(userId: string, movieId: string, movies: object, usersMap: object) {
        const user: User = usersMap[userId];
        if (user.hasRatedMovieById(movieId)) {
            return user.movieRateRecord[movieId];
        }
        // an expensive process
		// a clone of usersMap with normalize value (rate[movie] - averageRate)
        const avgRateUsersMap: object = {};
        for (const currentId in usersMap) {
            if (currentId === movieId) continue; 
            const currentUser: User = usersMap[currentId];
            const avgRateExcludeId: number = currentUser.getAveRageRatingExcludeId(movieId);
            avgRateUsersMap[currentId] = new User(currentId);
            for (const currentMovieId in currentUser.movieRateRecord) {
                const currentRate: number = currentUser.movieRateRecord[currentMovieId];
                avgRateUsersMap[currentId].movieRateRecord[currentMovieId] = currentRate - avgRateExcludeId;
            }
        }

		// this is calulate the similarty between the user with the rest
        const simUsers : Array<object> = await Promise.all(Object
            .keys(avgRateUsersMap)
            .filter(id => id !== userId && avgRateUsersMap[id].hasRatedMovieById(movieId))
            .map(async (id) => {
                const sim = await this.simWithNormalisedAvg(userId, id, movies, avgRateUsersMap);
                return {
                    sim,
                    user: avgRateUsersMap[id]
                }
            }));

        let pred: number = user.getAveRageRatingExcludeId(movieId);
        let diviend: number = 0;
        let divisor: number = 0;
		
		// K-nearest neighbor
        // sort all users by the similarity value and take the first 5 highest sim value
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 5)
            .forEach(obj => {
                diviend += (+obj['sim']*(obj['user'].getMovieRate(movies[movieId])));   
                divisor += Math.abs(+obj['sim']);
            });

        pred += (diviend / divisor);
        return +pred.toFixed(2);
    }
```
## item-based collaborative filtering approach

### Cosine similartiy for computing similarities between items
Implementation
```ts
async cosineSimilarityMeasure(movieIdA: string, movieIdB: string, usersMap: object) {
        if (movieIdA === '' || movieIdB === '') {
            return 0;
        }

        if (movieIdA === movieIdB) {
            return 1;
        }

        const ratedBothMoviesUsers: Array<User> = new Array<User>();

        for (const currentId in usersMap) {
            const currentUser = usersMap[currentId];
            if (currentUser.hasRatedMovieById(movieIdA) && currentUser.hasRatedMovieById(movieIdB)) {
                ratedBothMoviesUsers.push(currentUser);
            }
        }

        let diviend: number = 0;
        let divisorMovieA: number = 0;
        let divisorMovieB: number = 0;
        ratedBothMoviesUsers.forEach(user => {
            const currentAvgRate = user.getAverateRating();
            diviend += (user.getMovieRateById(movieIdA) - currentAvgRate)*(user.getMovieRateById(movieIdB) - currentAvgRate);
            divisorMovieA += (user.getMovieRateById(movieIdA) - currentAvgRate)*(user.getMovieRateById(movieIdA) - currentAvgRate);
            divisorMovieB += (user.getMovieRateById(movieIdB) - currentAvgRate)*(user.getMovieRateById(movieIdB) - currentAvgRate);
        });

        if (diviend !== 0 && divisorMovieA !== 0 && divisorMovieB !== 0) {
            return +(diviend / (Math.sqrt(divisorMovieA)*Math.sqrt(divisorMovieB))).toFixed(2);
        }

        return 0;
    }
```

### Prediction function
```ts
async predUserMovie(userId, movieId, usersMap: object) {
        const user: User = usersMap[userId];
        if (user.hasRatedMovieById(movieId)) {
            return user.movieRateRecord[movieId];
        }

        let diviend: number = 0;
        let divisor: number = 0;

        for (const currentMovieId in user.movieRateRecord) {
            if (currentMovieId === movieId) continue;

            const currentRatedMovieValue = user.movieRateRecord[currentMovieId];
            const simTwoMovies = await this.cosineSimilarityMeasure(movieId, currentMovieId, usersMap);
            diviend += simTwoMovies*currentRatedMovieValue;
            divisor += simTwoMovies;
        }
        
        if (diviend !== 0 && divisor !== 0) {
            return +(diviend / divisor).toFixed(2);
        }

        return 0;
}
```
