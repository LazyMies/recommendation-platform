# Assignment 2

### Project repository
https://gitlab.com/LazyMies/recommendation-platform
Require nodejs version 14
Npm version 6.14.*
Installation step:

 1. Clone the project https://gitlab.com/LazyMies/recommendation-platform.git
 2. Install the project by running `npm install`
 3. Start the server by running `npm start`

The project is ready to view at `http://localhost:3000`

### Info on the special data
Common data types like string, number, boolean are easy to understand. However, due to a technical issue in converting the Map object to JSON, I decided to use a default object instead of Map for retrieving data. 

`movies` is a map object mapping the `movieId` to an object type `Movie`. Here is an example of the `movies` map object.
```JSON
{
	"1": {
  		"movieId": "1",
  		"title": "Toy Story (1995)",
  		"genres": "Adventure|Animation|Children|Comedy|Fantasy"
	}
}
```

Similar to `movies`, `usersMap` is also a map object mapping the `userId` to an object type `User`. Here is an example of the `usersMap` map object.

```JSON
{
	"1": {
		"userId": "1",
  		"movieRateMap": {
			"1": 4,
			"3": 4,
			"6": 4,
			"47": 5,
			"50": 5,
			"70": 3
		}
	},
}
```


## A part
### Average Aggregation Method
Test page: http://localhost:3000/group-recommendation/average/movies?users=1,2,3,4
Implementation

```ts
async getRelevanceMoviesForUsers(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return [];
    const usersMap = await this.csvService.getUsersRecord();
    const movies = await this.csvService.getMoviesRecord();
    const movieIds = [...Array(15)]
            .map(_=> Math.ceil((Math.random()*Object.keys(movies).length) + 1))
            .map(id => id.toString())
            .filter(id => movies.hasOwnProperty(id));
    const listOfUsers = await Promise.all(
      users
        .split(',')
        .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersMap, movies))
    );
        

    const aggregateionList: Array<object> = new Array();
    movieIds.forEach(movieId => {
      let accumulateRate: number = 0;
      let result = {};
      for (let i = 0; i <listOfUsers.length; i++) {
        accumulateRate += listOfUsers[i].preds[`${movieId}`];
      }
      result = {
        movieId,
        rate: (+accumulateRate.toFixed(2)) / listOfUsers.length
      };
      aggregateionList.push(result);
    });
    aggregateionList.sort((a, b) => b['rate'] - a['rate'])
    
    return {
      movieIds,
      listOfUsers,
      aggregateionList
    };
  }
```

### Least Misery Method
Test page: http://localhost:3000/group-recommendation/least-misery/movies?users=1,2,3,4
Implementation
```ts
async getRelevanceMoviesForUsers(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return [];
    const usersMap = await this.csvService.getUsersRecord();
    const movies = await this.csvService.getMoviesRecord();
    const movieIds = [...Array(15)]
            .map(_=> Math.ceil((Math.random()*Object.keys(movies).length) + 1))
            .map(id => id.toString())
            .filter(id => movies.hasOwnProperty(id));
    const listOfUsers = await Promise.all(
      users
        .split(',')
        .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersMap, movies))
    );
        

    const aggregateionList: Array<object> = new Array();
    movieIds.forEach(movieId => {
      let result = {};
      
      const valuesFromUsers = listOfUsers.map(u => u.preds[movieId]).sort((a, b) => a - b);

      result = {
        movieId,
        rate: valuesFromUsers[0]
      };
      aggregateionList.push(result);
    });
    aggregateionList.sort((a, b) => b['rate'] - a['rate'])
    
    return {
      movieIds,
      listOfUsers,
      aggregateionList
    };
  }
```
## Part B

### Average Method considering Fairness
Test page: http://localhost:3000/group-recommendation/fairness/movies?users=1,2,3
Implementation
```ts
async getRelevanceMoviesForUsers(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return [];
    const usersMap = await this.csvService.getUsersRecord();
    const movies = await this.csvService.getMoviesRecord();
    const movieIds = [...Array(15)]
            .map(_=> Math.ceil((Math.random()*Object.keys(movies).length) + 1))
            .map(id => id.toString())
            .filter(id => movies.hasOwnProperty(id));
    const listOfUsers = await Promise.all(
      users
        .split(',')
        .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersMap, movies))
        .map(async (promiseObj) => {
            const user = await promiseObj; 
            return this.recommenderService.fairnessMeasure(user);
        })
    );

    const groupFairness: number = +((listOfUsers.map(user => user.fairness).reduce((prev, curr) => prev + curr)) / listOfUsers.length).toFixed(2);
        
    const aggregateionList: Array<object> = new Array();
    movieIds.forEach(movieId => {
      let accumulateRate :number = 0;
      for (let i = 0; i < listOfUsers.length; i++) {
        accumulateRate += listOfUsers[i].preds[movieId];
      }
      aggregateionList.push({
        movieId,
        rate: accumulateRate*groupFairness
      })
    });
    aggregateionList.sort((a, b) => b['rate'] - a['rate'])
    
    return {
      movieIds,
      listOfUsers,
      groupFairness,
      aggregateionList
    };
  }
```