# Assignment 3

### Project repository
https://gitlab.com/LazyMies/recommendation-platform
Require nodejs version 14
Npm version 6.14.*
Installation step:

 1. Clone the project https://gitlab.com/LazyMies/recommendation-platform.git
 2. Install the project by running `npm install`
 3. Start the server by running `npm start`

The project is ready to view at `http://localhost:3000`

### Info on the special data
Common data types like string, number, boolean are easy to understand. However, due to a technical issue in converting the Map object to JSON, I decided to use a default object instead of Map for retrieving data. 

`movies` is a map object mapping the `movieId` to an object type `Movie`. Here is an example of the `movies` map object.
```JSON
{
	"1": {
  		"movieId": "1",
  		"title": "Toy Story (1995)",
  		"genres": "Adventure|Animation|Children|Comedy|Fantasy"
	}
}
```

Similar to `movies`, `usersMap` is also a map object mapping the `userId` to an object type `User`. Here is an example of the `usersMap` map object.

```JSON
{
	"1": {
		"userId": "1",
  		"movieRateMap": {
			"1": 4,
			"3": 4,
			"6": 4,
			"47": 5,
			"50": 5,
			"70": 3
		}
	},
}
```

### Sequential Group Recommendation
Test page: http://localhost:3000/group-recommendation/sequential/movies?users=1,5,10
Implementation

```ts
const initArraysForIteration = (movies) => {
  const allMovieIds = Object.keys(movies);
  const movieIds = [];
  for (let i = 0; i < 5; i++) {
    const currentArray = [];
    for (let j = 0; j < 25; j++) {
      let randomIndex;
      let randomMvId;
      do {
        randomIndex = Math.ceil((Math.random()*allMovieIds.length));
        randomMvId = allMovieIds[randomIndex];
        if (i > 0) {
          const idExistInPrevArray = movieIds[i - 1].filter(mvId => mvId === randomMvId).length > 0;
          if (idExistInPrevArray) continue;
        }
      } while (allMovieIds.hasOwnProperty(randomMvId));
      currentArray.push(randomMvId);
    }
    movieIds.push(currentArray);
  }
  return movieIds;
};

const calculateGroupListSat = (movieIds, listOfUsers) => {
  let groupListSat: number = 0;
  movieIds.forEach(movieId => {
    for (let i = 0; i <listOfUsers.length; i++) {
      if (!listOfUsers[i].preds[movieId]) continue;
      groupListSat += listOfUsers[i].preds[movieId];
    }
  });
  return groupListSat;
};

const calculateUserListSat = (preds) => {
  let userListSat: number = 0;
  for (const mvId in preds) {
    if (!preds[mvId]) {
      userListSat += preds[mvId];
    }
  }
  return userListSat;     
}

async getRelevanceMoviesForUsers3(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return [];
    const movies = await this.csvService.getMoviesRecord();
    const usersMap = await this.csvService.getUsersRecord();
    const result = {};

    const movieIdsEachIteration = initArraysForIteration(movies);

    for (let i = 0; i < movieIdsEachIteration.length; i++) {
      const movieIds = movieIdsEachIteration[i];
      const listOfUsers = await Promise.all(
        users
          .split(',')
          .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersMap, movies))
      );
      
      const groupListSat = calculateGroupListSat(movieIds, listOfUsers);
      let accumulateUserSat: number = 0;
      // sort and get 20 highest rate movies
      // from 20 highest rate movie, calculate userListSat
      listOfUsers.forEach(u => {
        const sortedPreds = Object.entries(u.preds)
          .sort(([,a],[,b]) => b - a)
          .slice(0, 20)
          .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
        u.preds = sortedPreds;
        u['userListSat'] = calculateUserListSat(sortedPreds);
        u['sat'] = u['userListSat'] / groupListSat;
        accumulateUserSat += u['sat'];
      });
      
      const aggregateionList: Array<object> = new Array();
      movieIds.forEach(movieId => {
        let accumulateRate: number = 0;
        let result = {};
        for (let i = 0; i <listOfUsers.length; i++) {
          if (!listOfUsers[i].preds.hasOwnProperty(movieId)) continue;
          accumulateRate += listOfUsers[i].preds[`${movieId}`];
        }
        result = {
          movieId,
          rate: accumulateRate / listOfUsers.length
        };
        aggregateionList.push(result);
      });
      aggregateionList.sort((a, b) => b['rate'] - a['rate'])

      result[`iteration${i + 1}`] = {
        movieIds,
        listOfUsers,
        aggregateionList,
        groupListSat,
        groupSat: accumulateUserSat / users.length
      };
    }

    const satisfactionResult = {
      usersSatO: {},
      groupSatO: 0,
      groupDis: 0,
    };

    Object.keys(result).forEach(key => {
      const currentIteration = result[key];
      currentIteration.listOfUsers.forEach(user => {
        const currentUserId = user.userId;
        if (satisfactionResult.usersSatO.hasOwnProperty(currentUserId)) {
          satisfactionResult.usersSatO[currentUserId] += user.sat;
        } else {
          satisfactionResult.usersSatO[currentUserId] = user.sat;
        }
      });
    });

    Object.keys(satisfactionResult.usersSatO).forEach(uId => {
      satisfactionResult.usersSatO[uId] = +(satisfactionResult.usersSatO[uId] / Object.keys(result).length).toFixed(2)
      satisfactionResult.groupSatO += satisfactionResult.usersSatO[uId];
    });

    satisfactionResult.groupSatO = satisfactionResult.groupSatO / users.length;
    const allSatOs = Object.keys(satisfactionResult.usersSatO).map(uId => satisfactionResult.usersSatO[uId]);
    satisfactionResult.groupDis = allSatOs[allSatOs.length - 1] - allSatOs[0];

    return {
      result,
      satisfactionResult
    };
  }
```
