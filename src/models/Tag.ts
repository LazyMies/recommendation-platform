class Tag {
    public readonly userId: string;
    public readonly movieId: string;
    public readonly tag: string;
    public readonly timestamp: string;
}

export default Tag;