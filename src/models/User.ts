import Movie from "./Movie";

type MovieId = string;
type MovieRate = number;

class User {
    public userId: string;
    public movieRateRecord: Record<MovieId, MovieRate>;

    constructor(userId : string) {
        this.userId = userId;
        this.movieRateRecord = {};
    }

    getAverateRating(): number {
        let accumulateRate = 0;
        let count = 0;

        for (const currentId in this.movieRateRecord) {
            accumulateRate += this.movieRateRecord[currentId];
            count++;
        }
        
        if (accumulateRate > 0) {
            return accumulateRate / count;
        }

        return accumulateRate;
    };

    getAveRageRatingExcludeId(movieId: string) {
        let accumulateRate = 0;
        let count = 0;

        for (const currentId in this.movieRateRecord) {
            if (currentId === movieId) continue;
            accumulateRate += this.movieRateRecord[currentId];
            count++;
        }
        
        if (accumulateRate > 0) {
            return accumulateRate / count;
        }

        return accumulateRate;
    }

    getMovieRate(movie: Movie) : number {
        if (this.hasRatedMovie(movie)) {
            return this.movieRateRecord[movie.movieId];
        }

        return 0;
    }

    getMovieRateById(movieId: string) : number {
        if (this.hasRatedMovieById(movieId)) {
            return this.movieRateRecord[movieId];
        }
        
        return 0;
    }

    hasRatedMovie(movie: Movie) : boolean {
        return this.movieRateRecord.hasOwnProperty(`${movie.movieId}`);
    }

    hasRatedMovieById(movieId: string) : boolean {
        return this.movieRateRecord.hasOwnProperty(`${movieId}`);
    }
}

export default User;