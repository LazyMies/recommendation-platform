class Movie {
    public readonly movieId: string;
    public readonly title: string;
    public readonly genres: string;
}

export default Movie;