class Link {
    public readonly movieId: string;
    public readonly imdbId: string;
    public readonly tmdbId: string;
}

export default Link;