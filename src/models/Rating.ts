class Rating {
    public readonly userId: string;
    public readonly movieId: string;
    public readonly rating: string;
    public readonly timestamp: string;
}

export default Rating;