import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserRecommendationModule } from './modules/userrecommendation/userrecommendation.module';
import { GroupRecommendationModule } from './modules/grouprecommendation/grouprecommendation.module';
import { UserModule } from './modules/user/user.module';
import { MovieModule } from './modules/movie/movie.module';
import { RatingModule } from './modules/rating/rating';
import { DatabaseModule } from './modules/db/db.module';

@Module({
  imports: [
    UserRecommendationModule, 
    GroupRecommendationModule, 
    UserModule, RatingModule, 
    MovieModule, 
    // DatabaseModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
