import { Module } from '@nestjs/common';
import { DataModule } from '../csv/csv.module';
import { UserController } from './controllers/UserController';

@Module({
    imports: [DataModule],
    controllers: [UserController],
    providers: [],
})
export class UserModule {}