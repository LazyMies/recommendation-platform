import { Controller, Get, Param } from '@nestjs/common';
import User from 'src/models/User';
import { CsvService } from 'src/modules/csv/services/CsvService';

@Controller("users")
export class UserController {
    constructor(private csvService: CsvService) {}

    @Get()
    async getUsers(){
        const usersMap = await this.csvService.getUsersRecord();
        return {usersMap: usersMap};
    }

    @Get("/:id")
    async getUser(@Param("id") id: string) {
        const usersMap = await this.csvService.getUsersRecord();
        const currentUser = usersMap[id];
        return {
            user: {
                ...currentUser, 
                averageRating: +currentUser.getAverateRating().toFixed(2)
            }
        };
    }

    @Get("/movie/:id")
    async getUsersRateMovie(@Param("id") id: string) {
        const usersMap = await this.csvService.getUsersRecord();
        const users: Array<number> = new Array<number>();
        for (const currentId in usersMap) {
            const currentUser: User = usersMap[currentId];
            if (currentUser.hasRatedMovieById(id)) {
                users.push(+currentId);
            }
        }
        return users;
    }
}
