import { Module } from '@nestjs/common';
import { DataModule } from '../csv/csv.module';
import { MovieController } from './controllers/MovieController';

@Module({
    imports: [DataModule],
    controllers: [MovieController],
    providers: [],
})
export class MovieModule {}