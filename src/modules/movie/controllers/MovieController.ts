import { Controller, Get, Param } from '@nestjs/common';
import User from 'src/models/User';
import { CsvService } from 'src/modules/csv/services/CsvService';

@Controller("movie")
export class MovieController {
    constructor(private csvService: CsvService) {}

    @Get()
    async getMoviesRecord(){
        const movies = await this.csvService.getMoviesRecord();
        return movies;
    }

    @Get("/:id")
    async getUser(@Param("id") id: string) {
        const movies = await this.csvService.getMoviesRecord();
        if (movies.hasOwnProperty(id)) {
            return movies[id];
        }
        
        return {};
    }

    @Get("/:id/users")
    async getUsersRateMovie(@Param("id") id: string) {
        const usersMap = await this.csvService.getUsersRecord();
        const userIds: Array<string> = new Array();
        for (const userId in usersMap) {
            const currentUser: User = usersMap[userId];
            if (currentUser.hasRatedMovieById(id)) {
                userIds.push(userId);
            }
        }
        return userIds;
    }
}
