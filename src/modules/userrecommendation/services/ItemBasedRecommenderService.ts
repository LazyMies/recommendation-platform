import { Injectable } from "@nestjs/common";
import User from "src/models/User";
import { MovieId, UserId, UsersRecord } from "src/types/share-types";

@Injectable()
export class ItemBasedRecommenderService {
    constructor() {}

    async cosineSimilarityMeasure(movieIdA: string, movieIdB: string, usersRecord: UsersRecord) : Promise<number> {
        if (movieIdA === '' || movieIdB === '') {
            return 0;
        }

        if (movieIdA === movieIdB) {
            return 1;
        }

        const ratedBothMoviesUsers: Array<User> = new Array<User>();

        for (const currentId in usersRecord) {
            const currentUser = usersRecord[currentId];
            if (currentUser.hasRatedMovieById(movieIdA) && currentUser.hasRatedMovieById(movieIdB)) {
                ratedBothMoviesUsers.push(currentUser);
            }
        }

        let diviend: number = 0;
        let divisorMovieA: number = 0;
        let divisorMovieB: number = 0;
        ratedBothMoviesUsers.forEach(user => {
            const currentAvgRate = user.getAverateRating();
            diviend += (user.getMovieRateById(movieIdA) - currentAvgRate)*(user.getMovieRateById(movieIdB) - currentAvgRate);
            divisorMovieA += (user.getMovieRateById(movieIdA) - currentAvgRate)*(user.getMovieRateById(movieIdA) - currentAvgRate);
            divisorMovieB += (user.getMovieRateById(movieIdB) - currentAvgRate)*(user.getMovieRateById(movieIdB) - currentAvgRate);
        });

        if (diviend !== 0 && divisorMovieA !== 0 && divisorMovieB !== 0) {
            return +(diviend / (Math.sqrt(divisorMovieA)*Math.sqrt(divisorMovieB))).toFixed(2);
        }

        return 0;
    }

    async predUserMovie(userId: UserId, movieId: MovieId, usersRecord: UsersRecord): Promise<number> {
        const user: User = usersRecord[userId];
        if (user.hasRatedMovieById(movieId)) {
            return user.movieRateRecord[movieId];
        }

        let diviend: number = 0;
        let divisor: number = 0;

        for (const currentMovieId in user.movieRateRecord) {
            if (currentMovieId === movieId) continue;

            const currentRatedMovieValue: number = user.movieRateRecord[currentMovieId];
            const simTwoMovies: number = await this.cosineSimilarityMeasure(movieId, currentMovieId, usersRecord);
            diviend += simTwoMovies*currentRatedMovieValue;
            divisor += simTwoMovies;
        }
        
        if (diviend !== 0 && divisor !== 0) {
            return +(diviend / divisor).toFixed(2);
        }

        return 0;
    }
}