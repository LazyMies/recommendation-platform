import { Injectable } from "@nestjs/common";
import User from "src/models/User";
import { 
    UserId, 
    SimilarUser, 
    RelevantMovie, 
    SimilarUsers, 
    RelevantMovies, 
    SimilarUsersAndRelevantMovies,
    UsersRecord,
    MoviesRecord
} from "src/types/share-types";    

@Injectable()
export class UserBasedRecommenderService {
    constructor() {}

    async sim(userAId: string, userBId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<number> {
        let dividend: number = 0;
        let divisorA: number = 0;
        let divisorB: number = 0;
        const userA: User = usersRecord[userAId];
        const userB: User = usersRecord[userBId];

        const averateRatingUserA: number = userA.getAverateRating();
        const averateRatingUserB: number = userB.getAverateRating();

        for (const movieId in moviesRecord) {
            const movie = moviesRecord[movieId];
            if (userA.hasRatedMovie(movie) && userB.hasRatedMovie(movie)) {
                const rateOfUserA: number = userA.getMovieRate(movie);
                const rateOfUserB: number = userB.getMovieRate(movie);
                dividend += (rateOfUserA - averateRatingUserA) * (rateOfUserB - averateRatingUserB);
                divisorA += (rateOfUserA - averateRatingUserA) * (rateOfUserA - averateRatingUserA);
                divisorB += (rateOfUserB - averateRatingUserB) * (rateOfUserB - averateRatingUserB);
            }
        }

        if (dividend != 0 && divisorA != 0 && divisorB != 0) {
            const divisor = Math.sqrt(divisorA)*Math.sqrt(divisorB);
            return +(dividend / divisor).toFixed(4);
        }

        return 0;
    }

    // the sim prediction between userA and userB with the normalized rating value
    async simWithNormalisedAvg(userAId: string, userBId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord) : Promise<number> {
        let dividend: number = 0;
        let divisorA: number = 0;
        let divisorB: number = 0;
        const userA: User = usersRecord[userAId];
        const userB: User = usersRecord[userBId];

        for (const movieId in moviesRecord) {
            const movie = moviesRecord[movieId];
            if (userA.hasRatedMovie(movie) && userB.hasRatedMovie(movie)) {
                const rateOfUserA: number = userA.getMovieRate(movie);
                const rateOfUserB: number = userB.getMovieRate(movie);
                dividend += (rateOfUserA * rateOfUserB);
                divisorA += Math.pow(rateOfUserA, 2);
                divisorB += Math.pow(rateOfUserB, 2);
            }
        }

        if (dividend != 0 && divisorA != 0 && divisorB != 0) {
            const divisor = Math.sqrt(divisorA)*Math.sqrt(divisorB);
            return +(dividend / divisor).toFixed(4);
        }

        return 0;
    }

    async predUserMovie(userId: string, movieId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<number> {
        const user: User = usersRecord[userId];
        if (user.hasRatedMovieById(movieId)) {
            return user.movieRateRecord[movieId];
        }
        // an expensive process
        // a clone of usersRecord with normalize value (rate[movie] - averageRate)
        // going through every users and replace the rate value with the new formula (rate[movie] - averageRate)
        const avgRateUsersRecord: Record<UserId, User> = {};
        for (const currentId in usersRecord) {
            if (currentId === movieId) continue; 
            const currentUser: User = usersRecord[currentId];
            const avgRateExcludeId: number = currentUser.getAveRageRatingExcludeId(movieId);
            avgRateUsersRecord[currentId] = new User(currentId);
            for (const currentMovieId in currentUser.movieRateRecord) {
                const currentRate: number = currentUser.movieRateRecord[currentMovieId];
                avgRateUsersRecord[currentId].movieRateRecord[currentMovieId] = currentRate - avgRateExcludeId;
            }
        }

        // this is calulate the similarty between the user with the rest
        const usersRatedMovie: Array<string> = Object
            .keys(avgRateUsersRecord)
            .filter(id => id !== userId && avgRateUsersRecord[id].hasRatedMovieById(movieId));
        if (usersRatedMovie.length == 0) {
            return 0;
        }
        let simUsers : Array<object> = await Promise.all(
            usersRatedMovie
                .map(async (id) => {
                    const sim: number = await this.simWithNormalisedAvg(userId, id, moviesRecord, avgRateUsersRecord);
                    return {
                        sim,
                        user: avgRateUsersRecord[id]
                    }
                })
        );

        let pred: number = user.getAveRageRatingExcludeId(movieId);
        let diviend: number = 0;
        let divisor: number = 0;

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 5 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);
        if (simUsers.length > 5) {
            simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 5);
        }
        simUsers
            .forEach(obj => {
                diviend += (+obj['sim']*(obj['user'].getMovieRate(moviesRecord[movieId])));   
                divisor += Math.abs(+obj['sim']);
            });

        if (diviend == 0 && divisor == 0) {
            return 0;
        }

        pred += (diviend / divisor);
        return +pred.toFixed(2);
    }

    async similarUsersAndRelevantMovies(userId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<SimilarUsersAndRelevantMovies> {
        const similarUsers: Array<SimilarUser> = new Array();
        const relevantMovies: Array<RelevantMovie> = new Array();

        for(const currentId in usersRecord) {
            if (userId === currentId) continue;

            const sim = await this.sim(userId, currentId, moviesRecord, usersRecord);
            similarUsers.push({
                id: currentId,
                sim
            });
        }

        for (const currentMovieId in moviesRecord) {
            if (usersRecord[userId].hasRatedMovieById(currentMovieId)) continue;

            const pred = await this.predUserMovie(userId, currentMovieId, moviesRecord, usersRecord);
            relevantMovies.push({
                id: currentMovieId,
                pred
            });
        }

        return {
            similarUsers: similarUsers.sort((a,b) => b['sim'] - a['sim']).slice(0, 10),
            relevantMovies: relevantMovies.sort((a,b) => b['pred'] - a['pred']).slice(0, 20)
        }
    }

    async similarUsers(userId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<SimilarUsers> {
        const similarUsers: Array<SimilarUser> = await Promise.all(
            Object
                .keys(usersRecord)
                .filter(id => id !== userId)
                .map(async (id) => {
                    const sim = await this.sim(userId, id, moviesRecord, usersRecord);
                    return {
                        id,
                        sim
                    };
                })
        )

        return {
            similarUsers: similarUsers.sort((a,b) => b['sim'] - a['sim']).slice(0, 10)
        };
    }

    async relevantMovies(userId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<RelevantMovies> {
        const relevantMovies: Array<RelevantMovie> = await Promise.all(
            Object
                .keys(moviesRecord)
                .filter(id => !usersRecord[userId].hasRatedMovieById(id))
                .map(async (id) => {
                    const pred = await this.predUserMovie(userId, id, moviesRecord, usersRecord);
                    return {
                        id,
                        pred
                    };
                })
        );
        return {
            relevantMovies: relevantMovies.sort((a,b) => b['pred'] - a['pred']).slice(0, 20)
        }
    }
}