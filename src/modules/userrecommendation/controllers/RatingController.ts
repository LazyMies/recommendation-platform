import { Controller, Get } from '@nestjs/common';
import { ParsedData } from 'nest-csv-parser';
import Rating from 'src/models/Rating';
import { CsvService } from 'src/modules/csv/services/CsvService';

@Controller("ratings")
export class RatingController {
    constructor(private csvService: CsvService) {}

  @Get("count")
  async getRatingCount() {
    const ratings: ParsedData<Rating> = await this.csvService.getRatings();
    return ratings.total;
  }
}
