import { Controller, Get, Param, Render } from '@nestjs/common';
import { ItemBasedRecommenderService } from '../services/ItemBasedRecommenderService';
import { CsvService } from 'src/modules/csv/services/CsvService';
import { MovieId, UserId, UsersRecord } from 'src/types/share-types';

@Controller('user-recommendation/item-based')
export class ItemBasedController {
    constructor(
        private csvService: CsvService,
        private itemBasedrecommenderService: ItemBasedRecommenderService) {}

    @Get()
    @Render('item-based')
    async renderItemBasePrediction() {
        const userIds: Array<UserId> = await this.csvService.getUserIds();
        const movieIds: Array<MovieId> = await this.csvService.getMovieIds();
        return {
            userIds: userIds,
            movieIds: movieIds
        };
    }

    @Get('cosim/:movieIdA/:movieIdB')
    async getCosineSimilarityMeasure(@Param('movieIdA') movieIdA: string, @Param('movieIdB') movieIdB: string) {
        const usersRecords: UsersRecord = await this.csvService.getUsersRecord();
        const measure: number = await this.itemBasedrecommenderService.cosineSimilarityMeasure(movieIdA, movieIdB, usersRecords);
        return {
            value: measure
        };
    }

    @Get('pred/:userId/:movieId')
    async getItemBasedPredictUserMovie(@Param('userId') userId: string, @Param('movieId') movieId: string) {
        const usersRecords: UsersRecord = await this.csvService.getUsersRecord();
        const pred: number = await this.itemBasedrecommenderService.predUserMovie(userId, movieId, usersRecords);
        return {
            value: pred
        };
    }

}
