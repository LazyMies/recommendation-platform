import { Controller, Get, Render } from '@nestjs/common';

@Controller("user-recommendation")
export class MainController {
    constructor() {}

  @Get()
  @Render('user-recommendation')
  async getIndex(){
    return {
        message: 'Prediction Page',
    };
  }
}
