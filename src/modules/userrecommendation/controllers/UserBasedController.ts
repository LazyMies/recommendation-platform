import { Controller, Get, Param, Render } from '@nestjs/common';
import { UserBasedRecommenderService } from '../services/UserBasedRecommenderService';
import { CsvService } from 'src/modules/csv/services/CsvService';
import { MovieId, MoviesRecord, UserId, UsersRecord } from 'src/types/share-types';

@Controller('user-recommendation/user-based')
export class UserBasedController {
    constructor(
        private csvService: CsvService, 
        private userBasedrecommenderService: UserBasedRecommenderService) {}

    @Get()
    @Render('user-based')
    async renderUserBasePrediction() {
        const userIds: Array<UserId> = await this.csvService.getUserIds();
        const movieIds: Array<MovieId> = await this.csvService.getMovieIds();
        return {
            userIds: userIds,
            movieIds: movieIds
        };
    }

    @Get('sim/:id1/:id2')
    async getSimFromTwoUsers(@Param('id1') id1: string, @Param('id2') id2: string) {
        const usersRecords: UsersRecord = await this.csvService.getUsersRecord();
        const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
        const sim: number = await this.userBasedrecommenderService.sim(id1, id2, moviesRecord, usersRecords);
        return {
            value: sim
        };
    }

    @Get('pred/:userId/:movieId')
    async getUserBasedPredictUserMovie(@Param('userId') userId: string, @Param('movieId') movieId: string) {
        const usersRecords: UsersRecord = await this.csvService.getUsersRecord();
        const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
        const pred = await this.userBasedrecommenderService.predUserMovie(userId, movieId, moviesRecord, usersRecords);
        return {
            value: pred
        };
    }

    @Get('user/:userId')
    async getSimUsersAndRelMovies(@Param('userId') userId: string) {
        const usersRecords: UsersRecord = await this.csvService.getUsersRecord();
        const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
        const result = await this.userBasedrecommenderService.similarUsers(userId, moviesRecord, usersRecords);
        return result;
    }

    @Get('user/:userId/movies')
    async getRelevantMovies(@Param('userId') userId: string) {
        const usersRecords: UsersRecord = await this.csvService.getUsersRecord();
        const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
        const result = await this.userBasedrecommenderService.relevantMovies(userId, moviesRecord, usersRecords);
        return result;
    }
}
