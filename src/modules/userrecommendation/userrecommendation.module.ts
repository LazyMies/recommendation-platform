import { Module } from '@nestjs/common';
import { MainController } from './controllers/MainController';
import { UserBasedController } from './controllers/UserBasedController';
import { ItemBasedController } from './controllers/ItemBasedController';
import { RatingController } from './controllers/RatingController';
import { UserBasedRecommenderService } from './services/UserBasedRecommenderService';
import { ItemBasedRecommenderService } from './services/ItemBasedRecommenderService';
import { DataModule } from '../csv/csv.module';

@Module({
    imports: [DataModule],
    controllers: [MainController, UserBasedController, ItemBasedController, RatingController],
    providers: [UserBasedRecommenderService, ItemBasedRecommenderService],
})
export class UserRecommendationModule {
    constructor() {}
}