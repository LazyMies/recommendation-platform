import { Module } from '@nestjs/common';
import { DataModule } from '../csv/csv.module';
import { MainController } from './controllers/MainController';
import { AverageController } from './controllers/AverageController';
import { LeastMiseryController } from './controllers/LeastMiseryController';
import { FairnessController } from './controllers/FairnessController';
import { SequentialController } from './controllers/SequentialController';
import { RecommenderService } from './services/RecommenderService';
import { WhyNotController } from './controllers/WhyNotController';  

@Module({
    imports: [DataModule],
    controllers: [MainController, AverageController, LeastMiseryController, FairnessController, SequentialController, WhyNotController], 
    providers: [RecommenderService],
})
export class GroupRecommendationModule {
    constructor() {}
}