import { Controller, Get, Render, Query } from '@nestjs/common';
import { CsvService } from 'src/modules/csv/services/CsvService';
import { GroupAggregateResult, MovieId, MoviesRecord, UserRelevantMoviesRecord, UsersRecord } from 'src/types/share-types';
import { RecommenderService } from '../services/RecommenderService';

@Controller('group-recommendation/average')
export class AverageController {
    constructor(
      private readonly csvService: CsvService,
      private readonly recommenderService: RecommenderService) {}

  @Get()
  @Render('average')
  async getIndex(){
    return {
        message: 'Prediction Page',
    };
  }

  @Get('movies')
  async getRelevanceMoviesForUsers(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return {};
    const usersRecord: UsersRecord = await this.csvService.getUsersRecord();
    const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
    const movieIds: Array<MovieId> = [...Array(15)]
            .map(_=> Math.ceil((Math.random()*Object.keys(moviesRecord).length) + 1))
            .map(id => id.toString())
            .filter(id => moviesRecord.hasOwnProperty(id));
    const listOfUsers: Array<UserRelevantMoviesRecord> = await Promise.all(
      users
        .split(',')
        .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersRecord, moviesRecord))
    );
        
    const aggregationList: Array<object> = new Array();
    movieIds.forEach(movieId => {
      let accumulateRate: number = 0;
      let result = {};
      for (let i = 0; i <listOfUsers.length; i++) {
        accumulateRate += listOfUsers[i].preds[`${movieId}`];
      }
      result = {
        movieId,
        rate: (+accumulateRate.toFixed(2)) / listOfUsers.length
      };
      aggregationList.push(result);
    });
    aggregationList.sort((a, b) => b['rate'] - a['rate'])
    
    return {
      movieIds,
      listOfUsers,
      aggregationList
    };
  }
}
