import { Controller, Get, Render, Query } from '@nestjs/common';
import { CsvService } from 'src/modules/csv/services/CsvService';
import { RecommenderService } from '../services/RecommenderService';

@Controller('group-recommendation/fairness')
export class FairnessController {
    constructor(
      private readonly csvService: CsvService,
      private readonly recommenderService: RecommenderService) {}

  @Get()
  @Render('average')
  async getIndex(){
    return {
        message: 'Prediction Page',
    };
  }

  @Get('movies')
  async getRelevanceMoviesForUsers(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return [];
    const usersMap = await this.csvService.getUsersRecord();
    const movies = await this.csvService.getMoviesRecord();
    const movieIds = [...Array(15)]
            .map(_=> Math.ceil((Math.random()*Object.keys(movies).length) + 1))
            .map(id => id.toString())
            .filter(id => movies.hasOwnProperty(id));
    const listOfUsers = await Promise.all(
      users
        .split(',')
        .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersMap, movies))
        .map(async (promiseObj) => {
            const user = await promiseObj; 
            return this.recommenderService.fairnessMeasure(user);
        })
    );

    const groupFairness: number = +((listOfUsers.map(user => user.fairness).reduce((prev, curr) => prev + curr)) / listOfUsers.length).toFixed(2);
        
    const aggregateionList: Array<object> = new Array();
    movieIds.forEach(movieId => {
      let accumulateRate :number = 0;
      for (let i = 0; i < listOfUsers.length; i++) {
        accumulateRate += listOfUsers[i].preds[movieId];
      }
      aggregateionList.push({
        movieId,
        rate: accumulateRate*groupFairness
      })
    });
    aggregateionList.sort((a, b) => b['rate'] - a['rate'])
    
    return {
      movieIds,
      listOfUsers,
      groupFairness,
      aggregateionList
    };
  }
}
