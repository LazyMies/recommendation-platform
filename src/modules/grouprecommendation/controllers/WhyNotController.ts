import { Controller, Get, Render, Query, Param, Body, Post } from '@nestjs/common';
import Movie from 'src/models/Movie';
import { CsvService } from 'src/modules/csv/services/CsvService';
import { MovieId, 
  MovieRateGenre, 
  MoviesRecord, 
  UserId, 
  UserRelevantMoviesRecord, 
  UserRelevantMoviesRecordFairness, 
  UsersRecord, 
  WhyNotAtomResult, 
  WhyNotButResult, 
  WhyNotGroupListData, 
  WhyNotMovieTypeResult, 
  WhyNotRankHigherResult, 
  WhyNotRankHigherResultGroup, 
  WhyNotRankHigherThanResult } from 'src/types/share-types';
import { RecommenderService } from '../services/RecommenderService';

const initListOfMovieIds = (moviesRecord: MoviesRecord): Array<string> => {
    const allMovieIds: Array<string> = Object.keys(moviesRecord);
    const movieIds: Array<string> = [];
    for (let j = 0; j < 30; j++) {
        let randomIndex;
        let randomMvId;
        randomIndex = Math.ceil((Math.random()*allMovieIds.length));
        randomMvId = allMovieIds[randomIndex];
        movieIds.push(randomMvId);
    }
    return movieIds;
};

const avgAggregation = (movieIds: Array<MovieId>, moviesRecord: MoviesRecord, listOfUsers: Array<UserRelevantMoviesRecord> | Array<UserRelevantMoviesRecordFairness>): Array<MovieRateGenre> => {
  const aggregationList: Array<MovieRateGenre> = new Array();
  movieIds.forEach((movieId: MovieId) => {
    let accumulateRate: number = 0;
    let result: MovieRateGenre = {
      movieId,
      rate: 0,
      genre: moviesRecord[movieId].genres.indexOf('|') > -1 ? moviesRecord[movieId].genres.split('|')[0] : moviesRecord[movieId].genres
    };
    for (let i = 0; i <listOfUsers.length; i++) {
      accumulateRate += listOfUsers[i].preds[`${movieId}`];
    }
    result.rate = (+accumulateRate.toFixed(2)) / listOfUsers.length;
    aggregationList.push(result);
  });
  return aggregationList;
};

@Controller('group-recommendation/why-not')
export class WhyNotController {
    constructor(
      private readonly csvService: CsvService,
      private readonly recommenderService: RecommenderService) {}

  @Get()
  @Render('why-not')
  async getIndex(){
    const userIds: Array<UserId> = await this.csvService.getUserIds();
    return {
      userIds: userIds,
    };
  }

  @Get('movies')
  async getRelevanceMoviesForUsers(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return {};
    const usersRecord: UsersRecord = await this.csvService.getUsersRecord();
    const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
    const movieIds: Array<string> = initListOfMovieIds(moviesRecord);
    const listOfUsers: Array<UserRelevantMoviesRecord> = await Promise.all(
      users
        .split(',')
        .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersRecord, moviesRecord))
    );

    const aggregationList: Array<MovieRateGenre> = avgAggregation(movieIds, moviesRecord, listOfUsers);

    // calculate fairness for each user
    const listOfUsersFairness: Array<UserRelevantMoviesRecordFairness> = listOfUsers.map((user: UserRelevantMoviesRecord) => {
      let fairness: number = 0;
      let userSupportedPreferences: number = 0;
      for (const mvId in user.preds) {
        const mvRateInGroup: MovieRateGenre = aggregationList.find((mvRate: MovieRateGenre) => mvRate.movieId === mvId);
        if (!mvRateInGroup) continue;

        if (user.preds[mvId] >= mvRateInGroup.rate) userSupportedPreferences++;
      }

      fairness = userSupportedPreferences / aggregationList.length;
      return {
        ...user,
        fairness
      };
    });

    let totalUsersFairness: number = 0; 
    listOfUsersFairness.forEach((user: UserRelevantMoviesRecordFairness) => {
      totalUsersFairness+= user.fairness;
    });

    listOfUsersFairness.map((user: UserRelevantMoviesRecordFairness) => {
      for (const mvId in user.preds) {
        const currentRate = user.preds[mvId];
        const fairnessRate = currentRate * (1 + ((totalUsersFairness) / listOfUsersFairness.length) - user.fairness);
        user.preds[mvId] = +(fairnessRate.toFixed(2));
      }
    });
    // apply fairness to the final group
    const fairnessAggregationList: Array<MovieRateGenre> = avgAggregation(movieIds, moviesRecord, listOfUsersFairness);
    fairnessAggregationList.sort((a, b) => b['rate'] - a['rate']);

    const recommendedAggregationList: Array<MovieRateGenre> = fairnessAggregationList.slice(0, 20);
    const notRecommendedAggregationList: Array<MovieRateGenre> = fairnessAggregationList.slice(21, 30);

    const recommendedGenres: Set<string> = new Set(recommendedAggregationList.map((movieRateGenre: MovieRateGenre) => movieRateGenre.genre));
    const notRecommendedGenres: Set<string> = new Set(Object
      .values(moviesRecord)
      .map((movie: Movie) => movie.genres)
      .map((genres: string) => genres.indexOf('|') > -1 ? genres.split('|')[0] : genres)
      .filter((currentGenre: string) => !recommendedGenres.has(currentGenre)));
    
    return {
      movieIds,
      listOfUsers,
      aggregationList: fairnessAggregationList,
      recommendedAggregationList,
      notRecommendedAggregationList,
      notRecommendedGenres: Array.from(notRecommendedGenres.values())
    };
  }

  @Get('atom/:userId/:movieId')
  async getWhyNotSingleUserMovie(@Param('userId') userId: string, @Param('movieId') movieId: string) {
    const usersRecord: UsersRecord = await this.csvService.getUsersRecord();
    const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
    const whyNotResult: WhyNotAtomResult = await this.recommenderService.whyNotRecommendMovieToUser(userId, movieId, usersRecord, moviesRecord);
    return whyNotResult;
  }

  @Get('but/:userId/:movieId1/:movieId2')
  async getWhyNotMovieANotBut(@Param('userId') userId: string, @Param('movieId1') movieId1: string, @Param('movieId2') movieId2: string) {
    const usersRecord: UsersRecord = await this.csvService.getUsersRecord();
    const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
    const whyNotButResult: WhyNotButResult = await this.recommenderService.whySuggestBButNotA(userId, movieId1, movieId2, usersRecord, moviesRecord);
    return whyNotButResult;
  }

  @Post('rank-higher/:userId/:movieId')
  async getWhyNotMovieRankHigher(@Param('userId') userId: string, @Param('movieId') movieId: string, @Body() groupListData: WhyNotGroupListData) {
    const usersRecord: UsersRecord = await this.csvService.getUsersRecord();
    const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
    const whyNotRankHigherResult: WhyNotRankHigherResult = await this.recommenderService.whyNotRankHigher(userId, movieId, usersRecord, moviesRecord);
    const whyNotRankHigherResultGroup: WhyNotRankHigherResultGroup = this.recommenderService.whyNotRankHigherGroup(userId, movieId, groupListData, whyNotRankHigherResult);
    return whyNotRankHigherResultGroup;
  }

  @Get('rank-higher-than/:userId/:movieId1/:movieId2')
  async getWhyNotMovieRankHigherThan(@Param('userId') userId: string, @Param('movieId1') movieId1: string, @Param('movieId2') movieId2: string) {
    const usersRecord: UsersRecord = await this.csvService.getUsersRecord();
    const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
    const whyNotRankHigherThanResult: WhyNotRankHigherThanResult = await this.recommenderService.whyisNotAHigherThanB(userId, movieId1, movieId2, usersRecord, moviesRecord);
    return whyNotRankHigherThanResult;
  }

  @Get('movie-genre/:userId/:movieGenre')
  async getWhyNotMovieGenre(@Param('userId') userId: string, @Param('movieGenre') movieGenre: string) {
    const usersRecord: UsersRecord = await this.csvService.getUsersRecord();
    const moviesRecord: MoviesRecord = await this.csvService.getMoviesRecord();
    const whyNotMovieTypeResult: WhyNotMovieTypeResult = await this.recommenderService.whyNotMovieGenre(userId, movieGenre, usersRecord, moviesRecord);
    return whyNotMovieTypeResult;
  }
}
