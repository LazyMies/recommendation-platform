import { Controller, Get, Render, Param } from '@nestjs/common';
import { RecommenderService } from '../services/RecommenderService';
import { CsvService } from 'src/modules/csv/services/CsvService';

@Controller('group-recommendation')
export class MainController {
    constructor(private recommenderService: RecommenderService,
                private csvService: CsvService) {}

  @Get()
  @Render('group-recommendation')
  async getIndex(){
    return {
        message: 'Prediction Page',
    };
  }

  @Get('user/:userId/movie/:movieId')
  async getRelevance(@Param('userId') userId: string, @Param('movieId') movieId: string) {
    const usersMap = await this.csvService.getUsersRecord();
    const movies = await this.csvService.getMoviesRecord();
    const pred = await this.recommenderService.relevance(userId, movieId, usersMap, movies);
    return {
      value: pred
    };
  }
}
