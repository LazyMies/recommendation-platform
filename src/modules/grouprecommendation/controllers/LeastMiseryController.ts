import { Controller, Get, Render, Query } from '@nestjs/common';
import { CsvService } from 'src/modules/csv/services/CsvService';
import { RecommenderService } from '../services/RecommenderService';

@Controller('group-recommendation/least-misery')
export class LeastMiseryController {
    constructor(
      private readonly csvService: CsvService,
      private readonly recommenderService: RecommenderService) {}

  @Get()
  @Render('least-misery')
  async getIndex(){
    return {
        message: 'Prediction Page',
    };
  }

  @Get('movies')
  async getRelevanceMoviesForUsers(@Query('users') users: string) {
    if (users === '' || users.indexOf(',') < 0) return [];
    const usersMap = await this.csvService.getUsersRecord();
    const movies = await this.csvService.getMoviesRecord();
    const movieIds = [...Array(15)]
            .map(_=> Math.ceil((Math.random()*Object.keys(movies).length) + 1))
            .map(id => id.toString())
            .filter(id => movies.hasOwnProperty(id));
    const listOfUsers = await Promise.all(
      users
        .split(',')
        .map(async (userId) => await this.recommenderService.findUserRelevanceMovies(userId, movieIds, usersMap, movies))
    );
        

    const aggregateionList: Array<object> = new Array();
    movieIds.forEach(movieId => {
      let result = {};
      
      const valuesFromUsers = listOfUsers.map(u => u.preds[movieId]).sort((a, b) => a - b);

      result = {
        movieId,
        rate: valuesFromUsers[0]
      };
      aggregateionList.push(result);
    });
    aggregateionList.sort((a, b) => b['rate'] - a['rate'])
    
    return {
      movieIds,
      listOfUsers,
      aggregateionList
    };
  }
}
