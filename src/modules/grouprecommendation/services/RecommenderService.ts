import { Injectable } from "@nestjs/common";
import Movie from "src/models/Movie";
import User from "src/models/User";
import { 
    MovieId, 
    MoviePredictionValue, 
    MoviesRecord, 
    RelevantMoviesRecord, 
    UserId, 
    UserRelevantMoviesRecord, 
    UserRelevantMoviesRecordFairness, 
    UsersRecord, 
    UserWithSimValue, 
    WhyNotAtomResult, 
    WhyNotButResult, 
    WhyNotGroupListData, 
    WhyNotMovieTypeResult, 
    WhyNotRankHigherResult, 
    WhyNotRankHigherResultGroup, 
    WhyNotRankHigherThanResult} from "src/types/share-types";

const initAvgRateUsersRecord = (usersRecord: UsersRecord, excludeMvId: string) : UsersRecord => {
    const avgRateUsersRecord: UsersRecord = {};
    for (const currentId in usersRecord) {
        if (currentId === excludeMvId) continue; 
        const currentUser: User = usersRecord[currentId];
        const avgRateExcludeId: number = currentUser.getAveRageRatingExcludeId(excludeMvId);
        avgRateUsersRecord[currentId] = new User(currentId);
        for (const currentMovieId in currentUser.movieRateRecord) {
            const currentRate: number = currentUser.movieRateRecord[currentMovieId];
            avgRateUsersRecord[currentId].movieRateRecord[currentMovieId] = currentRate - avgRateExcludeId;
        }
    }
    return avgRateUsersRecord;
};

const initSimUsersForMovie = async (userId: string, usersRatedMovie: Array<UserId>, moviesRecord: MoviesRecord, avgRateUsersRecord: UsersRecord, simWithNormalisedAvg) => {
    return await Promise.all(
        usersRatedMovie
            .map(async (id) => {
                const sim: number = await simWithNormalisedAvg(userId, id, moviesRecord, avgRateUsersRecord);
                return {
                    sim,
                    user: avgRateUsersRecord[id]
                };
            })
    );
};

const getUserRatedMovieById = (usersRecord: UsersRecord, userId: UserId, movieId: MovieId) : Array<UserId> => {
    return Object
    .keys(usersRecord)
    .filter(id => id !== userId && usersRecord[id].hasRatedMovieById(movieId));
};

const getUserRatedMovieType = (usersRecord: UsersRecord, moviesRecord: MoviesRecord, userIdToExclude: UserId, movieGenre: string) : Array<UserId> => {
    return Object
    .keys(usersRecord)
    .filter(id => {
        const excludeCurrentId: boolean = id !== userIdToExclude; 
        const currentUserRatedMoviesRecord: Record<MovieId, number> = usersRecord[id].movieRateRecord;
        const currnetUserRatedMoviesGenre: Set<string> = new Set(
            Object
                .keys(currentUserRatedMoviesRecord)
                .map((currentMvId: MovieId) => moviesRecord[currentMvId])
                .map((currentMv: Movie) => currentMv.genres)
                .map((currentGenres: string) => currentGenres.indexOf('|') > -1 ? currentGenres.split('|')[0] : currentGenres)
        );
        return excludeCurrentId && currnetUserRatedMoviesGenre.has(movieGenre);
    });
};

@Injectable()
export class RecommenderService {
    constructor() {}

    private readonly LIKE_THRESHOLD: number = 3;

    async sim(userAId: string, userBId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<number> {
        let dividend: number = 0;
        let divisorA: number = 0;
        let divisorB: number = 0;
        const userA: User = usersRecord[userAId];
        const userB: User = usersRecord[userBId];

        const averateRatingUserA: number = userA.getAverateRating();
        const averateRatingUserB: number = userB.getAverateRating();

        for (const movieId in moviesRecord) {
            const movie: Movie = moviesRecord[movieId];
            if (userA.hasRatedMovie(movie) && userB.hasRatedMovie(movie)) {
                const rateOfUserA = userA.getMovieRate(movie);
                const rateOfUserB = userB.getMovieRate(movie);
                dividend += (rateOfUserA - averateRatingUserA) * (rateOfUserB - averateRatingUserB);
                divisorA += (rateOfUserA - averateRatingUserA) * (rateOfUserA - averateRatingUserA);
                divisorB += (rateOfUserB - averateRatingUserB) * (rateOfUserB - averateRatingUserB);
            }
        }

        if (dividend != 0 && divisorA != 0 && divisorB != 0) {
            const divisor = Math.sqrt(divisorA)*Math.sqrt(divisorB);
            return +(dividend / divisor).toFixed(4);
        }

        return 0;
    }

    async simWithNormalisedAvg(userAId: string, userBId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<number> {
        let dividend: number = 0;
        let divisorA: number = 0;
        let divisorB: number = 0;
        const userA: User = usersRecord[userAId];
        const userB: User = usersRecord[userBId];

        for (const movieId in moviesRecord) {
            const movie = moviesRecord[movieId];
            if (userA.hasRatedMovie(movie) && userB.hasRatedMovie(movie)) {
                const rateOfUserA = userA.getMovieRate(movie);
                const rateOfUserB = userB.getMovieRate(movie);
                dividend += (rateOfUserA * rateOfUserB);
                divisorA += Math.pow(rateOfUserA, 2);
                divisorB += Math.pow(rateOfUserB, 2);
            }
        }

        if (dividend != 0 && divisorA != 0 && divisorB != 0) {
            const divisor = Math.sqrt(divisorA)*Math.sqrt(divisorB);
            return +(dividend / divisor).toFixed(4);
        }

        return 0;
    }

    async predUserMovie(userId: string, movieId: string, moviesRecord: MoviesRecord, usersRecord: UsersRecord): Promise<number> {
        const user: User = usersRecord[userId];
        if (user.hasRatedMovieById(movieId)) {
            return user.movieRateRecord[movieId];
        }
        // an expensive process
        const avgRateUsersRecord: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId);

        const usersRatedMovie: Array<string> = Object
            .keys(avgRateUsersRecord)
            .filter(id => id !== userId && avgRateUsersRecord[id].hasRatedMovieById(movieId));
        if (usersRatedMovie.length == 0) {
            return 0;
        }
        let simUsers : Array<object> = await Promise.all(
            usersRatedMovie
                .map(async (id) => {
                    const sim: number = await this.simWithNormalisedAvg(userId, id, moviesRecord, avgRateUsersRecord);
                    return {
                        sim,
                        user: avgRateUsersRecord[id]
                    }
                })
        );

        let pred: number = user.getAveRageRatingExcludeId(movieId);
        let diviend: number = 0;
        let divisor: number = 0;

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);
        if (simUsers.length > 2) {
            simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);
        }
        simUsers
            .forEach(obj => {
                diviend += (+obj['sim']*(obj['user'].getMovieRate(moviesRecord[movieId])));   
                divisor += Math.abs(+obj['sim']);
            });

        if (diviend == 0 && divisor == 0) {
            return 0;
        }

        pred += (diviend / divisor);
        return +pred.toFixed(2);
    }

    async relevance(userId: string, movieId: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<Number> {
        if (usersRecord[userId].hasRatedMovieById(movieId)) {
            return usersRecord[userId].movieRateRecord[movieId];
        }

        const usersRatedMovie: UsersRecord = {};
        for (const currentId in usersRecord) {
            if (currentId === userId) continue;

            if (usersRecord[currentId].hasRatedMovieById(movieId)) {
                usersRatedMovie[currentId] = usersRecord[currentId];
            }
        }

        let diviend = 0;
        let divisor = 0;
        for (const currentId in usersRatedMovie) {
            const sim = await this.sim(userId, currentId, moviesRecord, usersRecord);
            const rateValue = usersRatedMovie[currentId].movieRateRecord[movieId];
            diviend += (sim*rateValue);
            divisor += sim;
        }

        if (diviend == 0 && divisor == 0) {
            return 0;
        }

        return +(diviend / divisor).toFixed(2);
    }

    async findUserRelevanceMovies(userId: string, movieIds: Array<string>, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<UserRelevantMoviesRecord> {
        let preds: RelevantMoviesRecord = {};

        for (let i = 0; i < movieIds.length; i++) {
            preds[movieIds[i]] = await this.predUserMovie(userId, movieIds[i], moviesRecord, usersRecord);
        }

        return {
            userId,
            preds
        };
    }
    
    fairnessMeasure(userWithPredMovies: UserRelevantMoviesRecord): UserRelevantMoviesRecordFairness {
        let fairness: number = 0;

        Object.keys(userWithPredMovies.preds).forEach((movieWithPred: MovieId) => {
            const currentPred: MoviePredictionValue = userWithPredMovies.preds[movieWithPred];
            if (currentPred >= this.LIKE_THRESHOLD && currentPred !== null) {
                fairness++;
            }
        });

        return {
            ...userWithPredMovies,
            fairness: fairness / Object.keys(userWithPredMovies.preds).length
        }
    }
    
    async whyNotRecommendMovieToUser(userId: string, movieId: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotAtomResult> {
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId);
        if (usersRatedMovie.length == 0) {
            return {
                result: `No one has rated this item`,
                tuples: []
            };
        }
        

        const avgRateUsersRecord: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId);
        // this is the Peers data
        let simUsers : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecord, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);

        // there is no one in the Peers
        if (simUsers.length == 0) {
            return {
                result: `None of your peers has rated this item.`,
                tuples: []
            };
        }

        // there are 1 or 2 users in the Peers
        if (simUsers.length <= 2) {
            return {
                result: `Only ${simUsers.length} (< 3) of user ${userId} peers has rated movie ${movieId}.`,
                tuples: []
            };
        }

        // sort the Peers and take the three (can be more , but in this case 3 is the simplified implementation)
        simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        // Generate the tuple data (userId, prediction data, similarity with the current User)
        const tuples = await Promise.all(
            simUsers.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        let result: string = '';

        // Setting the value that considering users like the movie is 3.5
        // Find all tuples with prediction data greater than 3.5
        const likeTuplesFilter = tuples.filter(tuple => tuple.score >= this.LIKE_THRESHOLD);

        if (likeTuplesFilter.length == 0) {
            result = 'All of your peers have given the item a low score.';
        } else if (likeTuplesFilter.length < tuples.length) {
            result = `${likeTuplesFilter.length} like ${movieId}, but ${tuples.length - likeTuplesFilter.length} dislike it`;
        }

        return {
            result,
            tuples
        };
    }

    async whySuggestBButNotA(userId: string, movieId1: string, movieId2: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotButResult> {
        // check if all users rated the certain movie
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId1);
        if (usersRatedMovie.length == 0) {
            return {
                result: `No one has rated this item ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }
        
        // find all Peers for movie1
        const avgRateUsersRecordMv1: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId1);
        let simUsersMv1 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv1, this.simWithNormalisedAvg);

        // find all Peers for movie2
        const avgRateUsersRecordMv2: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId2);
        let simUsersMv2 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv2, this.simWithNormalisedAvg);
        

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsersMv1 = simUsersMv1.filter(simUser => simUser['sim'] > 0);
        simUsersMv2 = simUsersMv2.filter(simUser => simUser['sim'] > 0);
        
        if (simUsersMv1.length == 0) {
            return {
                result: `Your most similar peers has not rated ${movieId1} but have rated ${movieId2}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        if (simUsersMv1.length <= 2) {
            return {
                result: `${simUsersMv2.length} of your peers have rated item ${movieId2} but only ${simUsersMv1.length} < (3) has rated item ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        simUsersMv1 = simUsersMv1
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        // should not cause error because it is a highly rated mv
        simUsersMv2 = simUsersMv2
        .sort((a, b) => +b['sim'] - +a['sim'])
        .slice(0, 3);

        const tuplesMv1 = await Promise.all(
            simUsersMv1.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId1, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        const tuplesMv2 = await Promise.all(
            simUsersMv2.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId2, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        return {
            result : `${simUsersMv2.length} peers like item ${movieId2} and ${simUsersMv1.length} dislike item ${movieId1}`,
            tuplesMv1,
            tuplesMv2
        };
    }

    async whyNotRankHigher(userId: string, movieId: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotRankHigherResult> {
        // checking if all users rated movies
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId);
        if (usersRatedMovie.length == 0) {
            return {
                result: `No one has rated this item`,
                tuples: []
            };
        }
        
        const avgRateUsersRecord: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId);
        // Finding Peers for current users based on users rated the movie
        let simUsers : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecord, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);
        if (simUsers.length == 0) {
            return {
                result: `None of your peers has rated this item.`,
                tuples: []
            };
        }

        if (simUsers.length <= 2) {
            return {
                result: `Only ${simUsers.length} (< 3) of user ${userId} peers has rated movie ${movieId}.`,
                tuples: []
            };
        }

        simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        const tuples = await Promise.all(
            simUsers.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        let result: string = '';
        const likeTuplesFilter = tuples.filter(tuple => tuple.score >= this.LIKE_THRESHOLD);

        if (likeTuplesFilter.length == 0) {
            result = `${simUsers.length} of your peers have given a low score to this item`;
        } else if (likeTuplesFilter.length < tuples.length) {
            result = `${likeTuplesFilter.length} like ${movieId}, but ${tuples.length - likeTuplesFilter.length} dislike it`;
        }

        return {
            result,
            tuples
        };
    }

    async whyisNotAHigherThanB(userId: string, movieId1: string, movieId2: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotRankHigherThanResult> {
        const usersRatedMovie: Array<UserId> = getUserRatedMovieById(usersRecord, userId, movieId1);
        if (usersRatedMovie.length == 0) {
            return {
                result: `Your peers have rated ${movieId2} but none of them have rated ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }
        
        const avgRateUsersRecordMv1: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId1);
        let simUsersMv1 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv1, this.simWithNormalisedAvg);

        const avgRateUsersRecordMv2: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId2);
        let simUsersMv2 : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovie, moviesRecord, avgRateUsersRecordMv2, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsersMv1 = simUsersMv1.filter(simUser => simUser['sim'] > 0);
        simUsersMv2 = simUsersMv2.filter(simUser => simUser['sim'] > 0);
        
        if (simUsersMv1.length == 0) {
            return {
                result: `Your most similar peers has not rated ${movieId1} but have rated ${movieId2}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        if (simUsersMv1.length <= 2) {
            return {
                result: `${simUsersMv2.length} of your peers have rated item ${movieId2} but only ${simUsersMv1.length} < (3) has rated item ${movieId1}`,
                tuplesMv1: [],
                tuplesMv2: []
            };
        }

        simUsersMv1 = simUsersMv1
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);

        // should not cause error because it is a highly rated mv
        simUsersMv2 = simUsersMv2
        .sort((a, b) => +b['sim'] - +a['sim'])
        .slice(0, 3);

        const tuplesMv1 = await Promise.all(
            simUsersMv1.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId1, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        const tuplesMv2 = await Promise.all(
            simUsersMv2.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId2, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );
        
        return {
            result : `${simUsersMv2.length} peers like item ${movieId2} and ${simUsersMv1.length} dislike item ${movieId1}`,
            tuplesMv1,
            tuplesMv2
        };
    }

    async whyNotMovieGenre(userId: string, movieGenre: string, usersRecord: UsersRecord, moviesRecord: MoviesRecord): Promise<WhyNotMovieTypeResult> {
        // Find all users rated movie with a certain genre
        const usersRatedMovieGenre: Array<UserId> = getUserRatedMovieType(usersRecord, moviesRecord, userId, movieGenre);
        if (usersRatedMovieGenre.length == 0) {
            return {
                result: `${movieGenre} hasnt had any movie rated`,
                tuples: []
            };
        }

        // find a movie id of that genre rated by the user
        const movie: Movie = 
            Object
                .keys(usersRecord[userId].movieRateRecord)
                .map((mvId: string) => moviesRecord[mvId])
                .find((currentMovie: Movie) => {
                    const currentMovieGenre = currentMovie.genres.indexOf('|') > -1 ? currentMovie.genres.split('|')[0] : currentMovie.genres;
                    return currentMovieGenre.indexOf(movieGenre) > -1;
                });
        if (!movie) {
            return {
                result: `You hasn't rated genre ${movieGenre}`,
                tuples: []
            };
        }

        const movieId = movie.movieId;
        const avgRateUsersRecord: UsersRecord = initAvgRateUsersRecord(usersRecord, movieId);
        // this is the Peers data
        let simUsers : Array<UserWithSimValue> = await initSimUsersForMovie (userId, usersRatedMovieGenre, moviesRecord, avgRateUsersRecord, this.simWithNormalisedAvg);

        // K-nearest neighbor
        // sort all users by the similarity value and take the first 3 highest sim value
        // filter all 0 value sim 
        // then for each of those, calculating the diviend & divisor based on the formula
        simUsers = simUsers.filter(simUser => simUser['sim'] > 0);
        if (simUsers.length == 0) {
            return {
                result: `None of your peers has rated ${movieGenre}`,
                tuples: []
            };
        }

        if (simUsers.length <= 2) {
            return {
                result: `Only ${simUsers.length} (< 3) of user ${userId} peers has rated movie ${movieGenre}.`,
                tuples: []
            };
        }

        // sort the Peers and take the three (can be more , but in this case 3 is the simplified implementation)
        simUsers = simUsers
            .sort((a, b) => +b['sim'] - +a['sim'])
            .slice(0, 3);


        // Generate the tuple data (userId, prediction data, similarity with the current User)
        const tuples = await Promise.all(
            simUsers.map(async (simUser) => {
                const currentUser: User = simUser['user'];
                const predMovie: number = await this.predUserMovie(currentUser.userId, movieId, moviesRecord, usersRecord);
                return {
                    userId: currentUser.userId, 
                    score: predMovie, 
                    sim: simUser.sim
                };
            })
        );

        let result: string = '';
        // Setting the value that considering users like the movie is 3.5
        // Find all tuples with prediction data greater than 3.5
        const likeTuplesFilter = tuples.filter(tuple => tuple.score >= this.LIKE_THRESHOLD);

        if (likeTuplesFilter.length == 0) {
            result = `Your peers dislike ${movieGenre}`;
        } else if (likeTuplesFilter.length < tuples.length) {
            result = `Only ${likeTuplesFilter.length} like ${movieGenre}.`;
        }

        return {
            result,
            tuples
        };
    }

    whyNotRankHigherGroup(userId: string, movieId: string, groupListData: WhyNotGroupListData, whyNotRankHigher: WhyNotRankHigherResult) : WhyNotRankHigherResultGroup{
        // initialise array of user rating that certain movie id
        const userMovieRates = groupListData.listOfUsers.map(user => {
            return {
                userId: user.userId,
                movieId: movieId,
                rate: user.preds[movieId]
            };
        });

        let groupResult = '';

        // sort by the rate value in ascendency order
        userMovieRates.sort((a, b) => b.rate - a.rate);

        
        if (userMovieRates[0].userId === userId) {
            // if the current user is in the first
            groupResult = `Other users did not rate the movie ${movieId} as high as you.`;
        } else if (userMovieRates[userMovieRates.length - 1]) {
            // if the current user is in the first
            groupResult = `You did not rate the movie ${movieId} as high as other group members.`;
        } else {
            // in the middle
            groupResult = `The averate rage for the movie ${movieId} is not as high enough`;
        }

        return {
            ...whyNotRankHigher,
            groupResult
        }
    }
}