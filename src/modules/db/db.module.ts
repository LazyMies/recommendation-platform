import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DataModule } from '../csv/csv.module';

import { MainController } from './controllers/MainController';
import { DatabaseService } from './services/DatabaseService';
import { UserSchema, User } from './schemas/user.schema';

@Module({
    providers: [DatabaseService],
    exports: [DatabaseService],
    controllers: [MainController],
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        // MongooseModule.forRoot('mongodb://localhost:27017/recommendation-system'),
        DataModule
    ]
})
export class DatabaseModule { 

}