import { Injectable } from "@nestjs/common";
import { InjectConnection, InjectModel } from "@nestjs/mongoose";
import { Connection, Model } from "mongoose";
import { User, UserDocument } from "../schemas/user.schema";

class UserDto {
    readonly id: string;
    readonly movieRateRecord: Record<string, number>;
}

@Injectable()
export class DatabaseService {
    constructor(
        @InjectModel(User.name) private userModel: Model<UserDocument>,
        @InjectConnection() private connection: Connection) {}

    async create(user: object): Promise<User> {
        const createdUser = new this.userModel(user);
        return createdUser.save();
    }   
}