import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
    @Prop()
    userId: string;

    @Prop(raw({
        movieId: { type: String },
        movieRate: { type: Number }
    }))
    movieRateRecord: Record<string, any>;
}

export const UserSchema = SchemaFactory.createForClass(User);