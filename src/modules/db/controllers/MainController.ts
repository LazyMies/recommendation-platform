import { Controller, Get, Param } from '@nestjs/common';
import { CsvService } from 'src/modules/csv/services/CsvService';
import { DatabaseService } from '../services/DatabaseService';

@Controller("db")
export class MainController {
    constructor(
        private csvService: CsvService,
        private dbService: DatabaseService) {}

    @Get()
    getIndex() {
        return "Hello World";
    }

    @Get("user/:id")
    async getUser(@Param("id") id: string) {
        const usersMap = await this.csvService.getUsersRecord();
        const createdUser = await this.dbService.create(usersMap[id]);
        return {
            userFromCsv: usersMap[id],
            createdUser
        };
    }
}
