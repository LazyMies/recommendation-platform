import { Module } from '@nestjs/common';
import { MainController } from './controllers/MainController';
import { DataModule } from '../csv/csv.module';

@Module({
    imports: [DataModule],
    controllers: [MainController],
    providers: [],
})
export class RatingModule {
    constructor() {}
}