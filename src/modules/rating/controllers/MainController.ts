import { Controller, Get, Render } from '@nestjs/common';
import { CsvService } from 'src/modules/csv/services/CsvService';

@Controller('ratings')
export class MainController {
    constructor(private csvService: CsvService) {}

  @Get('')
  @Render('ratings')
  async getRatingCount() {
    const ratings = await this.csvService.getRatings();
    const users = await this.csvService.getUsersRecord();
    const movies = await this.csvService.getMoviesRecord();
    return {
      total: ratings.total,
      users: Object.keys(users).map(uId => users[uId]).map(u => JSON.stringify(u, undefined, 2)).slice(0, 2),
      movies: Object.keys(movies).map(mId => movies[mId]).map(mv => JSON.stringify(mv, undefined, 2).trim()).slice(0, 5)
    };
  }
}
