import { Module } from '@nestjs/common';
import { CsvService } from './services/CsvService';
import { CsvModule } from 'nest-csv-parser';

@Module({
    providers: [CsvService],
    exports: [CsvService],
    imports: [CsvModule]
})
export class DataModule { 

}