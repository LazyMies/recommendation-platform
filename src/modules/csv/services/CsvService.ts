import { Injectable } from "@nestjs/common";
import { CsvParser, ParsedData } from "nest-csv-parser";
import Rating from "src/models/Rating";
import Movie from "src/models/Movie";
import User from "src/models/User";
import { join } from 'path';
import * as fs from "fs";
import { MovieId, MoviesRecord, UserId } from "src/types/share-types";

type UserRecord = Record<string, User>;

@Injectable()
export class CsvService {
    static RATINGS_CSV_PATH: string = join(__dirname, "..", "..", "..", "..", "/resources/ratings.csv");
    static MOVIES_CSV_PATH: string = join(__dirname, "..", "..", "..", "..", "/resources/movies.csv");

    constructor(
        private readonly csvParser: CsvParser
    ) {

    }

    async getRatings(): Promise<ParsedData<Rating>> {
        const stream: fs.ReadStream = fs.createReadStream(CsvService.RATINGS_CSV_PATH);
        const ratings: ParsedData<Rating> = await this.csvParser.parse(stream, Rating);
        return ratings;
    }

    async getMoviesRecord() : Promise<MoviesRecord> {
        const stream: fs.ReadStream = fs.createReadStream(CsvService.MOVIES_CSV_PATH);
        const parsedDataMovies: ParsedData<Movie> = await this.csvParser.parse(stream, Movie, null, null, {
            separator: ','
        });

        const movies: MoviesRecord = {};
        parsedDataMovies.list.forEach(movie => {
            movies[movie.movieId] = movie;
        });

        return movies;
    }

    async getUsersRecord() : Promise<UserRecord> {
        const stream: fs.ReadStream = fs.createReadStream(CsvService.RATINGS_CSV_PATH);
        const ratings: ParsedData<Rating> = await this.csvParser.parse(stream, Rating, null, null, {
            separator: ','
        });
        const usersMap : UserRecord = {};

        ratings.list.forEach(rating => {
            const currentUserId: string = rating.userId;
            let currentUser: User;

            if (usersMap.hasOwnProperty(currentUserId)) {
                currentUser = usersMap[currentUserId];
            } else {
                currentUser = new User(currentUserId);
                usersMap[currentUserId] = currentUser;
            }

            if (!currentUser.hasRatedMovieById(rating.movieId)) {
                currentUser.movieRateRecord[rating.movieId] = parseInt(rating.rating, 10);
            }
        });
        
        return usersMap;
    }

    async getUserIds(): Promise<Array<UserId>> {
        const usersMap = await this.getUsersRecord();
        return Object.keys(usersMap);
    }

    async getMovieIds(): Promise<Array<MovieId>> {
        const movies = await this.getMoviesRecord();
        return Object.keys(movies);
    }
}