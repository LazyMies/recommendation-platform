export function eachInMap ( map, block ) {
   let output = '';
   for (const [ key, value ] of map) {
      output += block.fn({ key, value });
   }
   return output;
 }