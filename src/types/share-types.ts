import Movie from "src/models/Movie";
import User from "src/models/User";

export type UserId = string;
export type MovieId = string;
export type SimilarValue = number;
export type MoviePredictionValue = number;
export type SimilarUser = {
    id: UserId,
    sim: SimilarValue,
};
export type RelevantMovie = {
    id: MovieId, 
    pred: MoviePredictionValue
};
export type SimilarUsers = {
    similarUsers: Array<SimilarUser>
};
export type RelevantMovies = {
    relevantMovies: Array<RelevantMovie>
};
export type SimilarUsersAndRelevantMovies = {
    similarUsers: Array<SimilarUser>,
    relevantMovies: Array<RelevantMovie>
}
export type UsersRecord = Record<UserId, User>;
export type MoviesRecord = Record<MovieId, Movie>;
export type RelevantMoviesRecord = Record<MovieId, MoviePredictionValue>;
export type UserRelevantMoviesRecord = {
    userId: UserId,
    preds: RelevantMoviesRecord
};
export type UserRelevantMoviesRecordFairness = {
    userId: UserId,
    preds: RelevantMoviesRecord,
    fairness: number
}

export type MovieRateGenre = {
    movieId: MovieId,
    rate: number,
    genre: string
}

export type GroupAggregateResult = {
    movieIds: Array<number>,
    listOfUsers: Array<UserRelevantMoviesRecord>,
    aggregateionList: Array<MovieRateGenre>
}

export type UserWithSimAndMoviePred = {
    userId: UserId,
    score: number,
    similarity: number
}

export type WhyNotAtomResult = {
    result: string,
    tuples: any | []
}

export type WhyNotButResult = {
    result: string,
    tuplesMv1: any | [],
    tuplesMv2: any | []
}

export type WhyNotRankHigherResult = {
    result: string,
    tuples: any | []
}

export type WhyNotRankHigherThanResult = {
    result: string,
    tuplesMv1: any | [],
    tuplesMv2: any | []
}

export type WhyNotMovieTypeResult = {
    result: string,
    tuples: any | []
}

export type WhyNotGroupListData = {
    movieIds: Array<number>,
    listOfUsers: Array<UserRelevantMoviesRecord>,
    aggregationList: Array<MovieRateGenre>
    recommendedAggregationList: Array<MovieRateGenre>,
    notRecommendedAggregationList: Array<MovieRateGenre>,
    notRecommendedGenres: Array<string>
}

export type UserWithSimValue = {
    sim: number,
    user: User
}

export type WhyNotRankHigherResultGroup = {
    result: string,
    groupResult: string,
    tuples: any | []
}